# Written by foo@gnusocial.net under GNU/GPLv3 license.   
# This software uses python-wxpython libraries for work.    

Features:   
Autopaste torrents/magnets links copied on clipboard   
Quiet and removefiles include   
All players options supported.    
Allows subtitles file use

foo:~$ peerflix -h   
Usage: /usr/local/bin/peerflix magnet-link-or-torrent [options]   
   
Options:   
  -c, --connections  max connected peers                            [default: 100]   
  -p, --port         change the http port                           [default: 8888] 
  -i, --index        changed streamed file (index)                
  -l, --list         list available files with corresponding index  
  -t, --subtitles    load subtitles file                          
  -q, --quiet        be quiet                                     
  -v, --vlc          autoplay in vlc*                             
  -s, --airplay      autoplay via AirPlay                         
  -m, --mplayer      autoplay in mplayer*                         
  -g, --smplayer     autoplay in smplayer*                        
  --mpchc            autoplay in MPC-HC player*                   
  --potplayer        autoplay in Potplayer*                       
  -k, --mpv          autoplay in mpv*                             
  -o, --omx          autoplay in omx**                            
  -w, --webplay      autoplay in webplay                          
  -j, --jack         autoplay in omx** using the audio jack       
  -f, --path         change buffer file path                      
  -b, --blocklist    use the specified blocklist                  
  -n, --no-quit      do not quit peerflix on vlc exit             
  -a, --all          select all files in the torrent              
  -r, --remove       remove files on exit                         
  -h, --hostname     host name or IP to bind the server to        
  -e, --peer         add peer by ip:port                          
  -x, --peer-port    set peer listening port                      
  -d, --not-on-top   do not float video on top                    
  --on-downloaded    script to call when file is 100% downloaded  
  --on-listening     script to call when server goes live         
  --version          prints current version          