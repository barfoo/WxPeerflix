#!/usr/bin/python


import wx
import os
import subprocess
import threading



class WxPeerflix(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'WxPeerflix beta 1.0', size=(1000, 520))
        panel = wx.Panel(self, -1) 
        self.torrentLabel = wx.StaticText(panel, -1, "Torrent/Magnet:", size=(150, -1))
        self.torrentText = wx.TextCtrl(panel, -1, "", size=(780, -1))
        self.torrentText.SetInsertionPoint(0)
        
      
        style = wx.TE_MULTILINE|wx.BORDER_SUNKEN|wx.TE_READONLY|wx.TE_RICH2
        
        players = ['None', 'VLC', 'mplayer', 'AirPlay', 'Potplayer', 'mpv', 'smplayer', 'mpchc', 'omx', 'webplay', 'jack'] 
        self.lst = wx.ListBox(panel, size = (200, 260), choices = players, style = wx.LB_SINGLE)

        self.quietCheckBox = wx.CheckBox(panel, -1, "quiet")
        self.removeCheckBox = wx.CheckBox(panel, -1, "remove files")
        self.subtitleCheckBox = wx.CheckBox(panel, -1, "subtitles")
        self.playerLabel = wx.StaticText(panel, -1, "Players:")
        self.subtitleText = wx.TextCtrl(panel, -1, "subtitles.srt", size=(300, -1))
        self.outLabel = wx.StaticText(panel, -1, "Command:")   
        self.btn = wx.Button(panel, -1, "Go")             
        self.outputText = wx.TextCtrl(panel, -1, style=style, size=(780, -1))
        sizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=20)
        sizer.AddMany([self.torrentLabel, self.torrentText, self.playerLabel, self.lst,
            self.removeCheckBox, self.quietCheckBox, self.subtitleCheckBox,self.subtitleText, self.outLabel, self.outputText, self.btn])
        panel.SetSizer(sizer)
        self.lst.Bind(wx.EVT_LISTBOX, self.onListBox) 
        self.btn.Bind(wx.EVT_BUTTON, self.onClickedGo) 
        self.quietCheckBox.Bind(wx.EVT_CHECKBOX, self.onCheckedQuiet) 
        self.removeCheckBox.Bind(wx.EVT_CHECKBOX, self.onCheckedRemove) 
        self.subtitleCheckBox.Bind(wx.EVT_CHECKBOX, self.onCheckedSubtitles) 
        self.magnet = ""
        self.option_remove = ""
        self.option_quiet = ""
        self.option_subtitles = ""
        #change this for default yours
        self.player = "VLC"
        self.player_option = " -v "
        self.subtitle_option = ""
        self.subtitle_file = ""
        self.clipPaste()
        self.link = ""
        self.command = ""
        
    def onClickedGo(self, event):
        if self.check_link(self.torrentText.GetValue()) and self.player != "":
            self.getPlayerOption()
            self.show_command()
            t = threading.Thread(target=self.outputCom, args=(self.command,))
            t.start()    
            
    def onCheckedQuiet(self, event):
        if self.quietCheckBox.GetValue():
           self.option_quiet = " --quiet "
        else:
           self.option_quiet = ""
        self.show_command()
        
    def onCheckedRemove(self, event):
        if self.removeCheckBox.GetValue():
           self.option_remove = " -r "
        else:
           self.option_remove = ""
        self.show_command()
        
    def onCheckedSubtitles(self, event):
        if self.subtitleCheckBox.GetValue():
           self.option_subtitles = " -t " + self.subtitleText.GetValue()
        else:
          self.option_subtitles = ""
        self.show_command()    
           
    def show_command(self):
        self.magnet = self.torrentText.GetValue()
        if self.magnet != "":
            self.command = "peerflix " + "'" + self.magnet + "'" + self.player_option + self.option_remove + self.option_quiet + self.option_subtitles 
            self.outputText.SetValue(self.command )
            self.outputText.Refresh()
        
   		
    def onListBox(self, event):
        self.player = event.GetEventObject().GetStringSelection()
        self.getPlayerOption() 
        self.show_command()
        
    def getPlayerOption(self):
        if self.player == "None":
            self.player_option = ""
        if self.player == "VLC":
            self.player_option = " -v "
        if self.player == "mplayer":
            self.player_option = " -m "
        if self.player == "AirPlay":
            self.player_option = " -s "
        if self.player == "Potplayer":
            self.player_option = " --potplayer "
        if self.player == "mpv":
            self.player_option = " -k "
        if self.player == "smplayer":
            self.player_option = " -g "
        if self.player == "mpchc":
            self.player_option = " --mpchc "
        if self.player == "omx":
            self.player_option = " -o "
        if self.player == "webplay":
            self.player_option = " -w "
        if self.player == "jack":
            self.player_option = " -j "                                                                                        
       
    
            
    def check_link(self, link):
        if link != "":                        
           if link[:7] == "magnet:":
              return True
                
           if link[-8:] == ".torrent":	
             return True
        else:
            self.outputText.SetValue("")
            self.outputText.Refresh()
            return False	    
		    
		    
    def outputCom(self, command):
        os.system(command)
        '''TODO? STDOUTPUT process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        #while 1:
        for line in process.stdout:
             #print line
             self.outputText.AppendText(line)
             self.outputText.AppendText("\n")
             #self.outputText.SetValue(line)
             self.outputText.Refresh()
             #sleep(0.5)'''
             
    def clipPaste(self):
       if not wx.TheClipboard.IsOpened():  
          do = wx.TextDataObject()
          wx.TheClipboard.Open()
          success = wx.TheClipboard.GetData(do)
          wx.TheClipboard.Close()
          if success:
             link = do.GetText()
             if self.check_link(link):
                 self.torrentText.SetValue(link)
                 
      
            

        
app = wx.App()
frame = WxPeerflix()
frame.Show()
app.MainLoop()
